package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

var Db *sql.DB

const (
	dbhost = "localhost"
	dbport = "5432"
	dbuser = "postgres"
	dbpass = "postgres"
	dbname = "ventas"
)

func InitDb() {

	var err error
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		dbhost, dbport,
		dbuser, dbpass, dbname)

	Db, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = Db.Ping()
	if err != nil {
		log.Println(err)
	}
	fmt.Println("Successfully connected!")
}
