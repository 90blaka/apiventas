package main

import (
	"encoding/json"
	"fmt"
)

//Devuelve arreglo de tipo Fruits...
type response2 struct {
	Fruits []string `json:"fruits"`
}
//Funcion principal..
func main() {
	str := `{"page": 1, "fruits": ["apple", "peach"]}`
	res := response2{}
	json.Unmarshal([]byte(str), &res)
	fmt.Println(str)
}

