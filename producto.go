package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

var items []Producto

// "Producto type" (tipo um objeto)
type Producto struct {
	Codigo    		string   `json:"codigo,omitempty"`
	Nombre    		string   `json:"nombre,omitempty"`
	Descripcion  	string   `json:"descripcion,omitempty"`
	Valor			int32	 `json:"valor,omitempty"`
	Categoria	  	string   `json:"categoria,omitempty"`
	Imagen			string	 `json:"imagen,omitempty"`
}

type listProducto struct {
	Prod []Producto `json:"catalogo-productos,omitempty"`
}

// GetProductos, trae todos los productos en formato Json
func GetProductos(w http.ResponseWriter, r *http.Request) {

	repos := listProducto{}

	err := getProductos(&repos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	out, err := json.Marshal(repos)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprintf(w, string(out))
}

//GetProductoCodigo, trae un producto en formato Json, pasando el codigo como parametro
func GetProductoCodigo(w http.ResponseWriter, r *http.Request) {

	prod := Producto{}

	err := getProductosCodigo(prod)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	out, err := json.Marshal(prod)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	fmt.Fprintf(w, string(out))

	/*params := mux.Vars(r)
	for _, item := range items {
		if item.Codigo == params["codigo"] {
			json.NewEncoder(w).Encode(item)
			return
		}
	}
	json.NewEncoder(w).Encode(&Producto{})*/
}

// função principal para executar a api
func main() {
	InitDb()
	router := mux.NewRouter()
	router.HandleFunc("/producto", GetProductos).Methods("GET")
	router.HandleFunc("/producto/{codigo}", GetProductoCodigo).Methods("GET")
	log.Fatal(http.ListenAndServe(":8000", router))
}

func getProductos(productos *listProducto) error {
	rows, err := Db.Query(`
		SELECT
			codigo,
			nombre,
			descripcion,
			valor,
			categoria,
			imagen
		FROM producto
		ORDER BY codigo DESC`)
	if err != nil {
		return err
	}
	defer rows.Close()
	for rows.Next() {
		prod := Producto{}
		err = rows.Scan(
			&prod.Codigo,
			&prod.Nombre,
			&prod.Descripcion,
			&prod.Valor,
			&prod.Categoria,
			&prod.Imagen,

		)
		if err != nil {
			return err
		}
		productos.Prod = append(productos.Prod, prod)
	}
	err = rows.Err()
	if err != nil {
		return err
	}
	return nil
}

func getProductosCodigo(data Producto) error {
	sqlStatement := `
        SELECT
			codigo,
			nombre,
			descripcion,
			valor,
			categoria,
			imagen
		FROM producto
        WHERE codigo=$1
        ORDER BY codigo DESC LIMIT 1;`

	row := Db.QueryRow(sqlStatement,"2")
	err := row.Scan(&data.Codigo,
		&data.Nombre,
		&data.Descripcion,
		&data.Valor,
		&data.Categoria,
		&data.Imagen)
	fmt.Println(data.Descripcion)
	if err != nil {
		return err
	}
	return nil
}
